function Get-Directory {
    # PSScriptRoot gets the path where
    Split-Path -Parent $PSScriptRoot
}
# get the current directory
$dir = Get-Directory

# create an array with the new paths
$new_paths = "$dir", "$dir\src" , "$dir\tests"

# get the environment path
$path = $Env:PYTHONPATH -split ";"

if ($path) {
    # the path exists already
    # join new paths to the existing one
    $env:PYTHONPATH = ($new_paths + $path) -join ";"
} else {
    # path does not exist
    # create it
    $env:PYTHONPATH = $new_paths -join ";"
}
