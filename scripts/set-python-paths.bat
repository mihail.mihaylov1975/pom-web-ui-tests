@echo off

:: set-python-paths.bat
:: First version for updating python paths on Windows

:: Initialize the python paths so that root directory get working.
:: Updated `py` to be in your PATH.
:: TODO for Linux`

if defined PYTHONPATH set _OLD_PYTHONPATH=%PYTHONPATH%
set PYTHONPATH=%CD%;%CD%\src;%CD%\tests;%_OLD_PYTHONPATH%
set _OLD_PYTHONPATH=
