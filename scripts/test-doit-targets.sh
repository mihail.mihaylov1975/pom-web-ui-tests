#! /bin/sh

set -e  # exit on error

doit test-dodo
doit clean-pycache
doit clean-pytest-cache
doit clean-temp
doit clean-all

doit cov-html || true
doit cov-term || true
#doit docs-html

doit lint || true
doit pylint || true

doit test  || true
#doit test-all  || true
doit test-offline  || true
doit test-unit  || true

doit show-env
doit venv-dev
doit init
doit patch-black
doit webdriver

echo OK
