#! /bin/bash

# init-venv.sh
#
# Initialize the environment so that `doit` targets get working. Requires
# `python3` to be in your PATH.
# On Windows, run `init-env.bat` or use GitBash to run this one. (You may need
# to remove old .venv bu hand if the script hangs after detecting the OS.)
#
# CAUTION! Wipes current virtual environment, if any.

set -e

echo -e '_______________________________________________________________\n'

if ! [ -d '.git' ]; then
  echo "Not in git repository root: $(pwd)"
  exit 2
fi

VENV_DIR='.venv'
os_name="$(uname -o)"

if [ "$os_name" = 'Msys' ]; then
  OS='Windows'
  VENV_BIN="$VENV_DIR/Scripts"
  LOG='..\venv-pip-install.log'
  ACTIVATE_CMD='.venv\Scripts\activate.bat'

elif [ "$os_name" = 'GNU/Linux' ]; then
  OS='Linux'
  VENV_BIN="$VENV_DIR/bin"
  LOG='/tmp/venv-pip-install.log'
  ACTIVATE_CMD='source .venv/bin/activate'

else
  echo "ERROR: Unsupported OS: $os_name!"
  exit 3
fi

echo "OS detected: $OS."

# CAUTION! Removes current virtual environment, if any:
rm -rf "$VENV_DIR"

echo 'Creating python virtualenv...'
venv_prompt="$(python3 -c 'import os; print(os.path.basename(os.getcwd()))')"
python3 -m venv "$VENV_DIR" --prompt "dirname($PWD)"

if ! [ -d "$VENV_BIN" ]; then
  echo ERROR: Unexpected .venv structure: No $VENV_BIN sub-directory!
  exit 3
fi

echo 'Updating virtualenv pip + setuptools...'
"$VENV_BIN/python3" -m pip install -U pip setuptools wheel >> $LOG

echo 'Populating virtualenv (dev requirements)...'
#python3 scripts/extract_req.py | "$VENV_BIN/pip" install -Ur /dev/stdin >> $LOG

"$VENV_BIN/pip" install -U doit >> $LOG
"$VENV_BIN/doit" init

cat <<EOS
_______________________________________________________________

Basic python virtual environment created which allows executing
'doit' build targets.

Location: $VENV_DIR
Full path: $(pwd)/$VENV_DIR
Log of this setup: $LOG

Activate the environment:
  \$ $ACTIVATE_CMD

All tragets can be seen via
  \$ doit list

Enjoy! ;)
EOS

source $VENV_BIN/activate
