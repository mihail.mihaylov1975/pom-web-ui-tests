"""
Default settings - ``config/default_url.py``.
Contains default url of the app.
"""

# URL used on CI job
BASE_URL = "https://www.google.bg/"
# Set headless option to True in order to drop CPU Usage on the CI
# This option should drop testing time, CPU usage and make the tests more stable.
HEADLESS = True
