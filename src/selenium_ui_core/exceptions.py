"""Define custom exceptions."""


class NoSuchURLPage(Exception):
    """Custom exception for missing url."""


class IncorrectBrowserChoice(Exception):
    """Exception for incorrect browser selection."""
