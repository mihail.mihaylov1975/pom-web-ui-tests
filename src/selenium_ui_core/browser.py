"""The Browser implementation.
Starts a new local session of different browsers e.g. firefox, chrome.
"""

import os

from typing import Dict, List
from selenium.common.exceptions import WebDriverException, NoSuchElementException, \
    TimeoutException,NoAlertPresentException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.action_chains import ActionChains

from .exceptions import NoSuchURLPage
from .browsers_factory import BrowserFactory


# pylint: disable=too-many-public-methods
class Browser:
    """Browser class."""

    def __init__(self):
        """Creates an instance for the browser."""
        self.driver = None

    def initialize(self, browser_name: str, user_agent: str, headless: bool) -> None:
        """Creates instance with different browsers e.g. firefox, chrome.
        :param browser_name: The name of the browser.
        :param user_agent: identity of a client
        :param headless: boolean value indicating to set the headless option
        """
        self.driver = BrowserFactory().get_browser(browser_name, user_agent, headless)

    def open_page(self, url: str, waiting_time: int = 5) -> None:
        """Loads a web page in the current browser session.

        :param url: URL of web page.
        :param waiting_time: Amount of time to wait implicitly (in seconds)
        """
        try:
            self.driver.implicitly_wait(waiting_time)
            self.driver.get(url)
        except WebDriverException as ex:
            raise NoSuchURLPage('No such web page: {}.'.format(url)) from ex

    def quit(self) -> None:
        """Quits the driver and close every associated window."""
        self.driver.quit()

    def close(self) -> None:
        """Closes the current window."""
        self.driver.close()

    def maximize_window(self) -> None:
        """ Maximizes the current window that webdriver is using."""
        self.driver.maximize_window()

    def find_element_and_wait_to_be_present(self,
                                            by: str,
                                            value: str,
                                            time_to_wait: int = 30) -> WebElement:
        """Find elements given a By strategy and locator.

        :param by: Type of locator.
        :param value: Locator value.
        :param time_to_wait: Amount of time to wait (in seconds)
        :return: WebElement once it is located.
        """
        web_element = WebDriverWait(self.driver, time_to_wait, poll_frequency=1).until(
            EC.presence_of_element_located((by, value)))
        return web_element

    def find_element_and_wait_to_be_visible(self,
                                            by: str,
                                            value: str,
                                            time_to_wait: int = 30) -> WebElement:
        """Find elements given a By strategy and locator.

        :param by: Type of locator.
        :param value: Locator value.
        :param time_to_wait: Amount of time to wait (in seconds)
        :return: WebElement once it is located.
        """
        web_element = WebDriverWait(self.driver, time_to_wait, poll_frequency=1).until(
            EC.visibility_of_element_located((by, value)))
        return web_element

    def find_elements_and_wait_to_be_present(self,
                                             by: str,
                                             value: str,
                                             time_to_wait: int = 30) -> List:
        """Find elements given a By strategy and locator.

        :param by: Type of locator.
        :param value: Locator value.
        :param time_to_wait: Amount of time to wait (in seconds).
        :return: list of WebElements once they are located.
        """
        web_elements = WebDriverWait(self.driver, time_to_wait, poll_frequency=1).until(
            EC.presence_of_all_elements_located((by, value)))
        return web_elements

    def find_elements_and_wait_all_to_be_visible(self, by: str,
                                                 value: str, time_to_wait: int = 30) -> List:
        """Find elements given a By strategy and locator.

        :param by: Type of locator.
        :param value: Locator value.
        :param time_to_wait: Amount of time to wait (in seconds).
        :return: the list of WebElements once they are located and visible.
        """
        web_elements = WebDriverWait(self.driver, time_to_wait, poll_frequency=1).until(
            EC.visibility_of_all_elements_located((by, value)))
        return web_elements

    def find_element_and_wait_to_be_clickable(
            self, by: str, value: str, time_to_wait: int = 30) -> WebElement:
        """Find elements given a By strategy and locator.

        :param by: Type of locator.
        :param value: Locator value.
        :param time_to_wait: Amount of time to wait (in seconds).
        :return: WebElement once it is located and clickable.
        """
        web_element = WebDriverWait(self.driver, time_to_wait, poll_frequency=1).until(
            EC.element_to_be_clickable((by, value)))
        return web_element

    def get_title(self) -> str:
        """Returns the title of the current page.
        :return: Title of web page -> str.
        """
        return self.driver.title

    def execute_script(self, script: str, *args) -> None:
        """Execute javascript script."""
        self.driver.execute_script(script, *args)

    def open_new_tab(self) -> None:
        """Open new tab in web browser."""
        self.execute_script("window.open('');")

    def switch_to_tab(self, tab: int) -> None:
        """Switches to the specified window.
        :param tab: window tab to switch.
        """
        self.driver.switch_to.window(self.driver.window_handles[tab])

    def close_browser_tab(self) -> None:
        """Closes the current window."""
        self.driver.close()

    def set_display_size(self, width: int, height: int) -> None:
        """Sets the width and height of the current window."""
        self.driver.set_window_size(width, height)

    def get_display_size(self) -> Dict:
        """Gets the width and height of the current window.
        :return: The resolution of the browser.
        """
        return self.driver.get_window_size()

    def save_screenshot(self, dir_path: str, filename: str = 'screenshot.png') -> None:
        """Saves a screenshot of the current window to a PNG image file.

        :param dir_path: Directory in which to save the screenshot.
        :param filename: The name of the screenshot file
        """
        self.driver.save_screenshot(os.path.join(dir_path, filename))

    @property
    def get_current_url(self) -> str:
        """Get current url of web page.
        :return: Url of the current web page.
        """
        return self.driver.current_url

    def get_user_agent(self) -> str:
        """Get the actual user agent that is used in Selenium.
        :return: Info about the current user agent.
        """
        user_agent = self.driver.execute_script("return navigator.userAgent;")
        return user_agent

    def implicitly_wait(self, time_to_wait):
        """Sets a sticky timeout to implicitly wait for an element to be found,
        or a command to complete. This method only needs to be called one
        time per session. To set the timeout for calls to
        execute_async_script, see set_script_timeout.
        :param time_to_wait: Amount of time to wait (in seconds)
        """
        self.driver.implicitly_wait(time_to_wait)

    def drag_and_drop(self, source_element, destination_element):
        """Holds down the left mouse button on the source element,
           then moves to the target element and releases the mouse button.
        :param source_element: The element to mouse down.
        :param destination_element: The element to mouse up.
        """
        ActionChains(self.driver).click_and_hold(source_element).move_to_element(
                destination_element).release(destination_element).perform()

    def drag_and_drop_by_offset(self, source_element, destination_element, xoffset, yoffset):
        """Holds down the left mouse button on the source element,
           then moves to the target element and releases the mouse button.
        :param source_element: The element to mouse down.
        :param destination_element: The element to mouse up.
        :param xoffset: The element to mouse up.
        :param yoffset: The element to mouse up.
        """
        if self.driver.name == 'firefox':
            action = ActionChains(self.driver)
            action.click_and_hold(source_element)
            action.move_to_element_with_offset(destination_element, xoffset, yoffset).pause(1)
            action.release().perform()
        else:
            action = ActionChains(self.driver)
            action.click_and_hold(source_element)
            action.move_to_element_with_offset(
                destination_element, xoffset, yoffset).pause(0.5).perform()
            action = ActionChains(self.driver)
            # Workaround for chrome the location of the element + 50
            action.move_to_element_with_offset(destination_element, xoffset, yoffset).pause(1)
            action.release().perform()

    def refresh(self) -> None:
        """Refreshes the current page."""
        self.driver.refresh()
        if self.driver.name == 'chrome':
            try:
                self.driver.switch_to.alert.accept()
            except NoAlertPresentException:
                pass

    def click_and_hold(self, element: WebElement, move_from: int, move_to: int) -> None:
        """Holds down the left mouse button on an element.
        :param element: The element to mouse down.
        :param move_from: X offset to move to, as a positive or negative integer.
        :param move_to: Y offset to move to, as a positive or negative integer."""
        move = ActionChains(self.driver)
        move.click_and_hold(element).move_by_offset(move_from, move_to).release().perform()

    def move_to_element(self, element_to: WebElement) -> None:
        """Moving the mouse to the middle of an element.
        :param element_to: The WebElement to move to."""
        ActionChains(self.driver).move_to_element(element_to).perform()

    def move_by_offset(self, element_to: WebElement) -> None:
        """Moving the mouse to the middle of an element.
        :param element_to: The WebElement to move to."""
        ActionChains(self.driver).move_by_offset(element_to.location['x'],
                                                 element_to.location['y']).perform()

    def double_click(self, on_element: WebElement) -> None:
        """Double-clicks an element.
        :on_element: The WebElement element to double-click."""
        action = ActionChains(self.driver)
        action.double_click(on_element).perform()

    def wait_until_element_is_not_present(self, element: WebElement, time_to_wait: int = 5):
        """Wait until an element is no longer present to the DOM"
        :param element: Web element to be hidden
        :param time_to_wait: time to wait in seconds"""
        WebDriverWait(self.driver, time_to_wait).until(EC.invisibility_of_element_located(element))

    def wait_until_url_to_be(self, url, time_to_wait: int = 10) -> None:
        """An expectation for checking the current url.
        :param url: Expected url
        :param time_to_wait: time to wait in seconds"""
        WebDriverWait(self.driver, time_to_wait).until(EC.url_to_be(url))

    def is_element_visible(self, by: str, value: str, time_to_wait: int = 30) -> bool:
        """Find elements given a By strategy and locator.
        :param by: Type of locator.
        :param value: Locator value.
        :param time_to_wait: Amount of time to wait (in seconds)
        :return: True if WebElement once it is located."""
        try:
            _ = WebDriverWait(self.driver, time_to_wait, poll_frequency=1).until(
                EC.visibility_of_element_located((by, value)))
            return True
        except NoSuchElementException:
            return False
        except TimeoutException:
            return False

    def get_cookies(self) -> List:
        """Returns a set of dictionaries, corresponding to cookies visible in
        the current session.
        :return: List of dictionaries, corresponding to cookies."""
        return self.driver.get_cookies()

    def delete_cookie(self, name: str) -> None:
        """Deletes a single cookie with the given name
        :param name: str cookie name."""
        self.driver.delete_cookie(name)

    def delete_all_cookies(self) -> None:
        """Delete all cookies in the scope of the session"""
        self.driver.delete_all_cookies()
