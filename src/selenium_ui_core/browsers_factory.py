"""A Browser Factory is one way of implementing a Page Object Model.
In order to support the Page Object pattern.
For more info see:
https://selenium-python.readthedocs.io/page-objects.html

Chrome + chromedriver seems particularly difficult to get running.
The 'no-sandbox' option is strongly recommended by knowledgeable
people -- see [1].

 .. [1] no-sandbox - https://stackoverflow.com/
 questions/53073411/selenium-webdriverexceptionchrome-failed-to-start-crashed-as-google-chrome-is
 opts.add_argument('--no-sandbox')
 opts.add_argument('--disable-dev-shm-usage')
"""

from selenium import webdriver
from selenium.webdriver.firefox.webdriver import WebDriver as FirefoxWebDriver
from selenium.webdriver.chrome.webdriver import WebDriver as ChromeWebDriver
from selenium.webdriver.edge.webdriver import WebDriver as EdgeWebDriver
from selenium.webdriver import ChromeOptions
from selenium.webdriver import FirefoxOptions
from selenium.webdriver import EdgeOptions

from .exceptions import IncorrectBrowserChoice

_DOWNLOAD_PATH = "/tmp"


class BrowserFactory:
    """Creates a new instance of given driver."""

    def get_browser(self, browser_name: str = 'firefox',
                    user_agent: str = 'desktop',
                    headless=True):
        """Starts the service and then creates new instance of given driver.

        :param browser_name: Browser name one of the following: firefox, chrome
        :return: New instance of chosen driver
        :param user_agent: identity of a client
        :param headless: boolean value indicating to set the headless option
        """
        if browser_name == 'firefox':
            return self.setup_firefox_browser(headless, user_agent)
        if browser_name == 'chrome':
            return self.setup_chrome_browser(headless, user_agent)
        if browser_name == 'edge':
            return self.setup_edge_browser(headless, user_agent)
        raise IncorrectBrowserChoice('Browser {} not supported.'.format(browser_name))

    @staticmethod
    def setup_firefox_browser(headless: bool, user_agent: str) -> FirefoxWebDriver:
        """Setup of firefox browser.
        :param headless: determines whether browser to be in headless mode
        :param user_agent: identity of a client
        """
        opts = FirefoxOptions()
        opts.headless = headless
        opts.set_preference('acceptInsecureCerts', True)
        opts.set_preference("browser.download.dir", _DOWNLOAD_PATH)
        opts.set_preference("network.dns.disableIPv6", False)
        opts.set_preference("extensions.logging.enabled", False)
        if user_agent == "tablet":
            opts.set_preference("general.useragent.override",
                                "Mozilla/5.0 (Android 12; Mobile; rv:68.0) "
                                "Gecko/68.0 Firefox/102.0")
        driver = webdriver.Firefox(options=opts)
        return driver

    @staticmethod
    def setup_chrome_browser(headless: bool, user_agent: str = 'desktop') -> ChromeWebDriver:
        """Setup of chrome browser.
        :param headless: determines whether browser to be in headless mode
        :param user_agent: identity of a client
        """
        opts = ChromeOptions()
        opts.headless = headless
        if headless:
            opts.add_argument("--headless=new")
        opts.add_experimental_option('excludeSwitches', ['enable-logging'])
        prefs = {"download.default_directory": _DOWNLOAD_PATH}
        opts.add_experimental_option("prefs", prefs)
        opts.add_argument('--ignore-certificate-errors')
        opts.add_argument('--disable-download-notification')
        opts.add_argument('--disable-popup-blocking')
        opts.add_argument("start-maximized")  # open Browser in maximized mode
        opts.add_argument("no-sandbox")
        if user_agent == "tablet":
            opts.add_argument('user-agent=Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 '
                              '(KHTML, like Gecko) Chrome/96.0.4664.104 Mobile Safari/537.36')
        driver = webdriver.Chrome(options=opts)
        return driver

    @staticmethod
    def setup_edge_browser(headless: bool, user_agent: str = 'desktop') -> EdgeWebDriver:
        """Setup of edge browser.
        :param headless: determines whether browser to be in headless mode
        :param user_agent: identity of a client
        """
        opts = EdgeOptions()
        opts.headless = headless
        opts.add_experimental_option('excludeSwitches', ['enable-logging'])
        prefs = {"download.default_directory": _DOWNLOAD_PATH}
        opts.add_experimental_option("prefs", prefs)
        opts.add_argument('--ignore-certificate-errors')
        opts.add_argument('--disable-download-notification')
        opts.add_argument('--disable-popup-blocking')
        opts.add_argument("start-maximized")  # open Browser in maximized mode
        opts.add_argument("no-sandbox")
        opts.add_argument("--disable-gpu")
        opts.add_argument("--disable-dev-shm-usage")
        if user_agent == "tablet":
            opts.add_argument('user-agent=Mozilla/5.0 (Linux; Android 10; HD1913) AppleWebKit'
                              '/537.36 (KHTML, like Gecko) Chrome/117.0.5938.60 '
                              'Mobile Safari/537.36 EdgA/116.0.1938.79')
        driver = webdriver.Edge(options=opts)
        return driver
