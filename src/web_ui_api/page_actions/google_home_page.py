"""Contains google page header action methods.
Part of page objects design pattern.
For more info see:
https://selenium-python.readthedocs.io/page-objects.html
"""
from selenium.webdriver.remote.webelement import WebElement

from selenium_ui_core.browser import Browser
from web_ui_api.page_models.google_home_page_artifacts import GooglePageSelectors


class GooglePage:
    """GooglePage class that contains action methods for google.bg page."""

    def __init__(self, driver: Browser):
        self.driver = driver

    @property
    def google_page_search_input(self) -> WebElement:
        """Find google_page_search_input web element.
        :return: google_page_search_input web element
        """
        return self.driver.find_element_and_wait_to_be_clickable(
            *GooglePageSelectors.GOOGLE_PAGE_SEARCH_INPUT)

    def click_google_page_search_input(self) -> None:
        """Click google_page_search_input web element."""
        self.google_page_search_input.click()
