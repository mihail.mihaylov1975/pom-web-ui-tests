"""Contains locators and locator values for topology-editor page."""

from selenium.webdriver.common.by import By


class GooglePageSelectors:
    """Class that contains locators for google page"""
    GOOGLE_PAGE_SEARCH_INPUT = By.CSS_SELECTOR, ".gLFyf"
