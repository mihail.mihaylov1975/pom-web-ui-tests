""" A module that contains the steps related to the google  page.
"""
# pylint: disable=no-name-in-module

from behave import given, then

from web_ui_api.page_actions.google_home_page import GooglePage

try:
    from config.local_url import BASE_URL
except ImportError:
    from config.default_url import BASE_URL


@given('I am on home page')
def step_open_google_url(context):
    """Open Google.bg URL."""
    context.driver.open_page(BASE_URL)
    context.driver.wait_until_url_to_be(BASE_URL)


@then('The page has title')
def step_verify_page_title(context):
    """Step verify title of the app."""
    assert context.driver.get_title() == "Google"


@then('Page has search text input')
def step_verify_google_page_has_search_input(context):
    """Step verify Page has search text input"""
    google_page = GooglePage(context.driver)
    assert google_page.google_page_search_input.is_displayed()
