"""Define code to run before and after certain events during the testing.
"""
import os

from behave.model import Status
from behave.model import Scenario
from behave.contrib.scenario_autoretry import patch_scenario_with_autoretry
import requests
from urllib3.exceptions import InsecureRequestWarning
from urllib3 import disable_warnings

from selenium_ui_core.browser import Browser

try:
    from config.local_url import HEADLESS, BASE_URL
except ImportError:
    from config.default_url import HEADLESS, BASE_URL


def browser_name(context):
    """Get browser name
    param: context: browser content
    return: browser name
    """
    return context.config.userdata['browser']


def current_user_agent(context):
    """Get current user agent.
    param: context: browser content
    return: current user agent
    """
    return context.config.userdata['user_agent']


def add_browser_name_to_junit_report_file(location, current_browser_name):
    """Add browser name to junit file name.
    param: location: location of the report files
    param: browser_name: the name of the browser to be added to report filename
    """
    for report_file in os.listdir(location):
        if report_file.endswith('.xml'):
            new_file = report_file[:6] + current_browser_name + '_' + report_file[6:]
            old_file_name = os.path.join(location, report_file)
            new_file_name = os.path.join(location, new_file)
            os.rename(old_file_name, new_file_name)


def is_chrome(context):
    """Check whether the current browser is chrome.
    param: context: browser content
    return: bool
    """
    return browser_name(context) == "chrome"


def is_tablet(context):
    """Check whether the current user agent is for tablet.
    param: context: browser content
    return: bool
    """
    return current_user_agent(context) == "tablet"


def is_desktop(context):
    """Check whether the current user agent is for desktop.
    param: context: browser content
    return: bool
    """
    return current_user_agent(context) == "desktop"


def is_max_size(context):
    """Check whether the current user agent is for desktop in max size.
    param: context: browser content
    return: bool
    """
    return current_user_agent(context) == "max_size"


def is_big_size(context):
    """Check whether the current user agent is for desktop in 1920, 1080.
    param: context: browser content
    return: bool
    """
    return current_user_agent(context) == "big_size"


# pylint: disable=broad-except
def is_url_exist():
    """Chek if the requested URL exist
    return: bool"""
    try:
        disable_warnings(InsecureRequestWarning)
        req = requests.get(BASE_URL, timeout=20, verify=False)
    except Exception:
        return False
    return req.status_code == 200


def before_all(context):
    """These run before the whole shooting match."""
    # set behave to continue executing steps even if any of steps fail
    Scenario.continue_after_failed_step = True
    # Store reports in subdirectory of reports directory
    context.config.junit_directory = os.path.join(context.config.junit_directory,
                                                  browser_name(context))
    if not os.path.isdir(context.config.junit_directory):
        os.makedirs(context.config.junit_directory)

    # Clean old junit report files
    for report_file in os.listdir(context.config.junit_directory):
        if str(report_file).endswith('.xml'):
            os.remove(os.path.join(context.config.junit_directory, report_file))


def after_all(context):
    """These run after the whole shooting match."""
    add_browser_name_to_junit_report_file(context.config.junit_directory,
                                          browser_name(context))


def before_feature(context, feature):
    """These run before each feature file is exercised."""
    # add auto retry for failing test cases except known fails
    if not is_url_exist():
        feature.skip("Marked with @skip. The requested URL does not exist.")
    for scenario in feature.scenarios:
        if 'known_fail' not in scenario.effective_tags:
            patch_scenario_with_autoretry(scenario, max_attempts=3)
    if "tablet" in feature.tags and not is_tablet(context):
        feature.skip("Marked with @skip. The feature is only for tablet.")
        return
    if "desktop" in feature.tags and is_tablet(context):
        feature.skip("Marked with @skip. The feature is only for desktop.")
        return


def after_feature(context, feature):
    """These run after each feature file is exercised."""
    # Change the name of the feature for the Jenkins run because it has issue to create report for
    # tests with the same name
    feature.name = ' '.join([feature.name, browser_name(context)])


def before_scenario(context, scenario):
    """These run before each scenario is run."""
    context.driver = Browser()
    # See system requirements 4693924 Web Browser
    context.driver.initialize(browser_name(context),
                              current_user_agent(context),
                              HEADLESS)
    # See system requirements 4693924 Display resolution
    if is_tablet(context):
        context.driver.set_display_size(1280, 720)
    if is_max_size(context):
        context.driver.maximize_window()
    if is_big_size(context):
        context.driver.set_display_size(1920, 1080)
    else:
        context.driver.set_display_size(1280, 800)
    # Skip unstable test cases temporarily
    if "skip" in scenario.effective_tags:
        scenario.skip("Marked with @skip temporarily.")
        return
    # Skip test cases for chrome browser
    if "chrome_skip" in scenario.effective_tags and is_chrome(context):
        scenario.skip("Marked with @skip temporarily for chrome browser.")
        return
    # Skip specific test cases for desktop during tablet execution
    if "tablet" in scenario.effective_tags and not is_tablet(context):
        scenario.skip("Marked with @skip for desktop.")
        return
    # Skip test cases for tablet during desktop execution
    if "desktop" in scenario.effective_tags and is_tablet(context):
        scenario.skip("Marked with @skip for tablet.")
        return


# pylint: disable=unused-argument
def after_scenario(context, scenario):
    """These run after each scenario is run."""
    try:
        context.driver.quit()
        del context.driver
    except AttributeError:
        context.second_driver.quit()
        del context.second_driver


def after_step(context, step):
    """These run after each step is run."""
    if step.status == Status.failed:
        try:
            context.driver.save_screenshot(context.config.junit_directory, '{}_{}_failed.png'
                                           .format(step.name, browser_name(context)))
        except AttributeError:
            context.second_driver.save_screenshot(context.config.junit_directory, '{}_{}_failed.png'
                                                  .format(step.name,
                                                          browser_name(context)))
